<?php


namespace App\Services;


use App\Models\Order;
use App\Models\Comment;
use App\Models\Status;
use Exception;

class OrderService
{

    /**
     * @param string $description
     * @return Order
     * @throws Exception
     */
    public function create(string $description): Order
    {
        /** @var Status $status */
        $status = Status::query()->where('default', '=', true)->first();
        if (!$status) {
            throw new Exception('Default status not found.');
        }
        $order = new Order();
        $order->description = $description;
        $order->status_id = $status->id;
        $order->save();
        return $order;
    }

    /**
     * @param Order $order
     * @param int $statusId
     * @return Order
     */
    public function updateStatus(Order $order, int $statusId): Order
    {
        $order->status_id = $statusId;
        $order->save();
        return $order;
    }

    /**
     * @param Order $order
     * @param string $text
     * @return Comment
     */
    public function addComment(Order $order, string $text): Comment
    {
        /** @var Comment $comment */
        $comment = $order->comments()->create([
            'text' => $text,
        ]);
        return $comment;
    }

    /**
     * @param Order $order
     * @return void
     * @throws Exception
     */
    public function delete(Order $order): void
    {
        $order->delete();
    }
}
