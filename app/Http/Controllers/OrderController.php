<?php

namespace App\Http\Controllers;


use App\Http\Requests\Order\AddCommentRequest;
use App\Http\Requests\Order\CreateRequest;
use App\Http\Requests\Order\UpdateStatusRequest;
use App\Http\Resources\CommentResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Exception;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    private $orderService;

    /**
     * OrderController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param Order $order
     * @return JsonResponse
     * @throws Exception
     */
    public function get(Order $order)
    {
        return $this->response([
            'order' => new OrderResource($order),
        ]);
    }

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(CreateRequest $request)
    {
        $order = $this->orderService->create(
            $request->input('description')
        );

        return $this->response([
            'order' => new OrderResource($order),
        ]);
    }

    /**
     * @param Order $order
     * @param UpdateStatusRequest $request
     * @return JsonResponse
     */
    public function updateStatus(Order $order, UpdateStatusRequest $request)
    {
        $this->orderService->updateStatus(
            $order,
            $request->input('status_id')
        );

        return $this->response();
    }

    /**
     * @param Order $order
     * @param AddCommentRequest $request
     * @return JsonResponse
     */
    public function addComment(Order $order, AddCommentRequest $request)
    {
        $comment = $this->orderService->addComment(
            $order,
            $request->input('text')
        );

        return $this->response([
            'comment' => new CommentResource($comment)
        ]);
    }

    /**
     * @param Order $order
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(Order $order)
    {
        $this->orderService->delete($order);
        return $this->response();
    }
}
