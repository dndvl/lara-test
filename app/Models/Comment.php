<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class OrderComment
 * @property int $id
 * @property int $order_id
 * @property string $text
 * @package App\Models
 */
class Comment extends Model
{
    protected $fillable = [
        'id', 'order_id', 'text'
    ];
}
