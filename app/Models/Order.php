<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

/**
 * Class Order
 * @property int $id
 * @property string $description
 * @property int $status_id
 * @property Status $status
 * @property Comment[]|Collection $comments
 * @package App\Models
 */
class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'description', 'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
