<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class OrderStatus
 * @property int $id
 * @property string $name
 * @package App\Models
 */
class Status extends Model
{
    protected $fillable = [
        'id', 'name', 'default',
    ];
}
