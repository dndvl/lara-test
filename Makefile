include .env

up:
	docker-compose -f ./docker/docker-compose.yml -p asu_skc up -d --build --force-recreate

install:
	docker-compose -f ./docker/docker-compose.yml -p asu_skc exec api bash -c \
	"composer install && php artisan key:generate && php artisan migrate && php artisan optimize:clear"

test:
	docker-compose -f ./docker/docker-compose.yml -p asu_skc exec api bash -c "php artisan test"
