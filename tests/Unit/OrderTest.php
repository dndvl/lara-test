<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    public function testCreate()
    {
        // create order
        $response = $this->addOrderRequest();
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'order' => [
                'id',
                'description',
                'status' => [
                    'id',
                    'name',
                ],
                'comments',
            ]
        ]);
        $response->assertJson([
            'order' => [
                'status' => [
                    'id' => 1,
                ],
            ],
        ]);
    }

    public function testUpdateStatus()
    {
        // create order
        $order = $this->addOrder();

        // update status
        $response = $this->postJson('/api/order/' . $order->id . '/status', [
            'status_id' => 2,
        ]);
        $response->assertStatus(200);

        // check status
        $response = $this->getJson('/api/order/' . $order->id);
        $response->assertJson([
            'order' => [
                'status' => [
                    'id' => 2,
                ],
            ],
        ]);
    }

    public function testAddComment()
    {
        // create order
        $order = $this->addOrder();

        // add comment
        $response = $this->postJson('/api/order/' . $order->id . '/comment', [
            'text' => 'Hello!',
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'comment' => [
                'id',
                'text',
            ]
        ]);

        // count comments
        $response = $this->getJson('/api/order/' . $order->id);
        $response->assertJsonCount(1, 'order.comments');
    }

    public function testDelete()
    {
        // create order
        $order = $this->addOrder();

        // delete order
        $response = $this->deleteJson('/api/order/' . $order->id);
        $response->assertStatus(200);

        // exists order
        $response = $this->getJson('/api/order/' . $order->id);
        $response->assertStatus(404);
    }


    protected function addOrderRequest()
    {
        return $this->postJson('/api/order/', [
            'description' => 'test'
        ]);
    }

    protected function addOrder()
    {
        $response = $this->addOrderRequest();
        $data = json_decode($response->getContent());
        return $data->order;
    }
}
