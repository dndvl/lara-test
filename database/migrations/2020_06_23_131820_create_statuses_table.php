<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('default')->default(false);
            $table->timestamps();
        });

        DB::table('statuses')->insert([
            [
                'name' => 'Новая',
                'default' => true,
            ],
            [
                'name' => 'В работе',
                'default' => false,
            ],
            [
                'name' => 'Решена',
                'default' => false,
            ],
            [
                'name' => 'Отклонена',
                'default' => false,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
    }
}
