<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('order')->group(function() {
    Route::post('/', 'OrderController@create');
    Route::get('{order}', 'OrderController@get');
    Route::delete('{order}', 'OrderController@delete');
    Route::post('{order}/status', 'OrderController@updateStatus');
    Route::post('{order}/comment', 'OrderController@addComment');
});
